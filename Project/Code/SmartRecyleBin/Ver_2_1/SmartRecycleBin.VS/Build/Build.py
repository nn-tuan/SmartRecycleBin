import glob
import os
import time
import traceback
import argparse

print("Build creating...\n===================================================\n")

def GetParameter():
    error = False

    parser = argparse.ArgumentParser()
    parser.add_argument('--cpl', dest='cpl', type=str, help='Add compiler:\nEx:"C:/Program Files/Microchip/xc8/v2.40/bin/xc8.exe"')
    parser.add_argument('--chip', dest='chip', type=str, help='Add chip name:\nEx: --chip=16F877')
    parser.add_argument('--out', dest='out', type=str, help='Add output path:\nEx: D:/OutputPath/FolderName')
    parser.add_argument('--main', dest='main', type=str, help='Add main path:\nEx: D:/MainPath/main.c')
    parser.add_argument('--rebuild', dest='rebuild', type=str, help='Recreat c_cpp_properites.json file and tasks.json file')
    args = parser.parse_args()

    print ("Compiler:       ", args.cpl)
    print ("Chip:           ", args.chip)
    print ("Output Folder:  ", args.out)
    print ("Main file:      ", args.main)
    print ("Rebuild:        ", args.rebuild)

    compile_path = args.cpl
    chip = args.chip
    out = args.out
    main_path = args.main
    rebuild = args.rebuild
    if rebuild == None:
        rebuild = "NONE"

    if compile_path == None or chip == None or out == None or main_path == None:
        error = True

    return error, compile_path, chip, out, main_path, rebuild


def get_data_file(line_check, file_path=""):
    """
    Hàm lấy dữ liệu từ file. 
    + line_check: dòng có chứa data cần lấy dữ liệu.
    + file_path: đường dẫn tới file cần đọc.
    """

    result = "NOTFOUND"
    check_string = False
    try:
        file = open(file_path, 'r+', encoding= 'utf-8')
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            if Find(data_file, line_check):
                print("Data search:", data_file)
                result = data_file
                pass
            if(file_cursor == file.tell()):
                break
        pass
    finally:
        file.close()
    # print("KetQua:", result)
    return result      

# Hàm tìm chuỗi str trong chuỗi data. Nếu có trả về True, ngược lại trả về False.
def Find(data, str):
    """
    Hàm giúp tìm chuỗi str trong chuỗi data. 
    + Nếu tìm thấy trả về True.
    + Nếu không tìm thấy trả về False.
    """
    if (data.find(str) > -1):
        return 1
    else:
        return 0

def respace_data_file(line_check, data_replace, file_path):
    list_data = []

    try:
        file = open(file_path, 'r+', encoding= 'utf-8')
        while True:
            file_cursor = file.tell()
            data_file = file.readline()
            
            if Find(data_file, line_check):
                list_data.append(data_replace)
            else:
                list_data.append(data_file)
            
            if(file_cursor == file.tell()):
                break
        pass
        file.close()

        file = open(file_path, 'w+', encoding= 'utf-8')
        for line in list_data:
            file.write(line)
    finally:
        file.close()
    pass


def create_cpp_file(path):
    c_cpp_json = """{
    "configurations": [
        
        {
            "name": "Windows",
            "includePath": [
                "${workspaceFolder}",
                "{path}/pic/include/*",
                "{path}/pic/include/legacy/*",
                "{path}/pic/include/c99/*"
                
            ],
            "defines": [],
            "intelliSenseMode": "clang-x64",
            "browse": {
                "path": [
                    "${workspaceFolder}",
                    "{path}/pic/include/*",
                    "{path}/pic/include/legacy/*",
                    "{path}/pic/include/c99/*"
                ],
                "databaseFilename": "",
                "limitSymbolsToIncludedHeaders": true
            },
            "cStandard": "c89",
            "compilerPath": "{path}/pic/bin/cpp.exe"
        }
    ],
    "version": 4
}"""
    c_cpp_json = c_cpp_json.replace("{path}", path)
    return c_cpp_json

def creat_task_file(path):
    tasks_json = """{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        {
            "label": "Recreat & Build",
            "type": "process",
            "windows": {
                "command": "./Build/Build.cmd",
                "args": [
                    // Fix Compiler URL in here:
                    "{path}/bin/xc8.exe",
                    "{BuildPath}",
                    "16F877",
                    "NONE",
                    "NONE", 
                    "TRUE"
                ]
            },
            "problemMatcher": [],
            "group": {
                "kind": "build",
                "isDefault": true
            }
        },
        {
            "label": "Build (Recommend)",
            "type": "process",
            "windows": {
                "command": "./Build/Build.cmd",
                "args": [
                    "{path}/bin/xc8.exe",
                    "{BuildPath}",
                    "16F877",
                    "NONE",
                    "NONE", 
                    "NONE"
                ]
            },
            "problemMatcher": [],
            "group": {
                "kind": "build",
                "isDefault": true
            }
        }
    ]
}"""
    tasks_json = tasks_json.replace("{path}", path)
    tasks_json = tasks_json.replace("{BuildPath}", r"Build\\dist\\Build.exe")
    return tasks_json
    

def FixLib():
    try:
        src_path = os.getcwd()
        src_path = src_path.split("Build")[0]
        print("Path:", src_path)
        file_path = src_path + "\Peripheral_Libs\Hearder\LoopProcess.h"
        include_str = get_data_file("Peripheral_Libs\Hearder\define.h", file_path)
        respace_data_file(include_str, "    #include \"" + src_path + "\Peripheral_Libs\Hearder\define.h\"\n", file_path)
        return src_path
    except:
        print("ERORR:")
        print(traceback.format_exc())
        return False



def UploadCmd(src_path, compile_path, chip, out, main_path):
    try:
        
        if out == None or out.upper() == "NONE":
            out = src_path + "\Bin"
        if main_path == None or main_path.upper() == "NONE":
            main_path = src_path + "\main.c"

        ECHO = "echo off\n"
        MKDIR = "DEL /F /Q " + out + "\nmkdir " + out + "\n"
        PATH_XC = "\"" + compile_path +"\" " 
        CHIP = "--chip=" + chip + " --std=c90 "
        OUTPUT =  "--outdir=\"" + out + "\" "
        FILE_MAIN = "\"" + main_path + "\" " 

        all_files = glob.glob(os.path.join(src_path + "\Peripheral_Libs\Source\\", "*.c"))

        # Create train.txt
        with open(src_path + "\Build\Build.cmd", "w") as f:
            f.write(ECHO)
            f.write("%2" + " --cpl=%1 --chip=%3 --out=%4 --main=%5 --rebuild=%6" + "\n")
            f.write(MKDIR + PATH_XC + CHIP + OUTPUT + FILE_MAIN)
            for idx in range(0, len(all_files)):
                f.write("\"" + all_files[idx] + "\"" + " ")
                print("File:", all_files[idx])
            f.write("--MSGDISABLE=359,1273,1388 --OPT=all ")
        print("Build.cmd created!\n===================================================\n")
    except:
        print("ERORR:")
        print(traceback.format_exc())


def main():
    error, cpl, chip, out, main_path, rebuild = GetParameter()
    
    print ("Compiler:       ", cpl)
    print ("Chip:           ", chip)
    print ("Output Folder:  ", out)
    print ("Main file:      ", main_path)
    print ("Rebuild:        ", rebuild)
    cpl = cpl.replace("\\", "/")
    if rebuild.upper() == "TRUE":
        if error:
            if input("Entering a missing parameter may cause an exception!\nDo you want to continue processing?\n>Y/N?").upper() == "N":
                return False
        src_path = FixLib()
        try:
            file = open(src_path + "/.vscode/c_cpp_properties.json", 'w+', encoding= 'utf-8')
            file.write(create_cpp_file(cpl.replace("/bin/xc8.exe", "")))
            
        finally:
            file.close()
        try:
            file = open(src_path + "/.vscode/tasks.json", 'w+', encoding= 'utf-8')
            file.write(creat_task_file(cpl.replace("/bin/xc8.exe", "")))
            
        finally:
            file.close()
        pass
        UploadCmd(src_path=src_path, compile_path=cpl, chip=chip, out=out, main_path=main_path)
    else:
        if error:
            if input("Entering a missing parameter may cause an exception!\nDo you want to continue processing?\n>Y/N?").upper() == "N":
                return False

        src_path = FixLib()
        UploadCmd(src_path=src_path, compile_path=cpl, chip=chip, out=out, main_path=main_path)
    
    time.sleep(1)
    pass

main()