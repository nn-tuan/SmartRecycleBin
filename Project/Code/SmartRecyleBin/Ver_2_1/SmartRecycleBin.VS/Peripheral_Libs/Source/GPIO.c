#include "..\Hearder\GPIO.h"

//#define PA    *(unsigned char *)0x005
void GPIO_Write(volatile unsigned char *GPIO_Port, unsigned char Pin, uint8_t GPIO_State)
{
    if(GPIO_Port == &PORTA)
    {
        TRISA &= ~(1 << Pin);
    }
    else if(GPIO_Port == &PORTB)
    {
        TRISB &= ~(1 << Pin);
    }
    else if(GPIO_Port == &PORTC)
    {
        TRISC &= ~(1 << Pin);
    }
    else if(GPIO_Port == &PORTD)
    {
        TRISD &= ~(1 << Pin);
    }
    else
    {
        TRISE &= ~(1 << Pin);
    }
    switch(GPIO_State)
    {
        case HIGH:
            *GPIO_Port |= (unsigned char)(1 << Pin);
            break;
        case LOW:
            *GPIO_Port &= ~(1 << Pin);
            break;
        default:
            *GPIO_Port &= ~(1 << Pin);
            break;
    }
}

void GPIO_Write_Peripheral(Peripheral_Pin GPIO, uint8_t GPIO_State)
{
    if(GPIO.Port == &PORTA)
    {
        TRISA &= ~(1 << GPIO.Pin);
    }
    else if(GPIO.Port == &PORTB)
    {
        TRISB &= ~(1 << GPIO.Pin);
    }
    else if(GPIO.Port == &PORTC)
    {
        TRISC &= ~(1 << GPIO.Pin);
    }
    else if(GPIO.Port == &PORTD)
    {
        TRISD &= ~(1 << GPIO.Pin);
    }
    else
    {
        TRISE &= ~(1 << GPIO.Pin);
    }
    switch(GPIO_State)
    {
        case HIGH:
            *GPIO.Port |= (unsigned char)(1 << GPIO.Pin);
            break;
        case LOW:
            *GPIO.Port &= ~(1 << GPIO.Pin);
            break;
        default:
            *GPIO.Port &= ~(1 << GPIO.Pin);
            break;
    }
}

unsigned char GPIO_Read(volatile unsigned char *GPIO_Port, unsigned char Pin)
{
    if(*GPIO_Port & (1 << Pin))
    {
        return HIGH;
    }
    else
    {
        return LOW;
    }
    
}

unsigned char GPIO_Read_Peripheral(Peripheral_Pin GPIO)
{
    if(*GPIO.Port & (1 << GPIO.Pin))
    {
        return HIGH;
    }
    else
    {
        return LOW;
    }
    
}

void GPIO_Toggle(volatile unsigned char *GPIO_Port, unsigned char Pin)
{
    if(GPIO_Read(GPIO_Port, Pin))
    {
        GPIO_Write(GPIO_Port, Pin, LOW);
    }
    else
    {
        GPIO_Write(GPIO_Port, Pin, HIGH);
    }
}

void GPIO_Toggle_Period(volatile unsigned char *GPIO_Port, unsigned char Pin, uint16_t time_period, uint16_t duty)
{
    if(duty < time_period)
    {
        GPIO_Write(GPIO_Port, Pin, HIGH);
    }
    else
    {
        GPIO_Write(GPIO_Port, Pin, LOW);
    }
}