/* 
 * File:   Configuration.h
 * Author: tuang
 *
 * Created on April 16, 2022, 9:23 AM
 */

#include "..\Hearder\Configuration.h"
#include "..\Hearder\GPIO.h"
#include "..\Hearder\Timer.h"
// #include "..\Hearder\WatchdogTimer.h"
#include "..\Hearder\UART.h"
#include "..\Hearder\ADC.h"
#include "..\Hearder\HBrightCtrl.h"


void print_time(void)
{
    sprintf(TEST, "%d:%d:%d:%d\n", time.hour, time.min, time.sec, time.minisec);
    UART_WriteStr(TEST);
    memset(TEST, 0, sizeof(TEST));
}

void Startup_Infor(Peripheral_Pin *GPIO, uint8_t index)
{
    while (index--)
    {
        GPIO_Write(GPIO -> Port, GPIO -> Pin, HIGH);
        delay_ms(100);
        GPIO_Write(GPIO -> Port, GPIO -> Pin, LOW);
        delay_ms(100);
    }  
    CLRWDT();
}

void Reset_ADC_Register(void)
{
    /*RESET ADCON0 and ADCON1 Register*/
    ADCON0 = 0x00;
    ADCON1 = 0x00;
    /*SET all PIN in PORTA to Digital PIN*/
    ADCON1 |= (7 << 0);
}

void MCU_Config(void)
{
    uint16_t timeout = 1000;
    /*=========== SET STARTUP VALUE ============*/
    // GPIO_Write(LED2.Port, LED2.Pin, HIGH);

    /*Timer0 config with interrupt mode:*/
    Timer2_Interrupt_Init();
    
    /*Reset ADC Register: Analog => Digital PIN mode*/
    Reset_ADC_Register();
    
    /*UART Configuration:*/
    UART_BASE_Init(9600);
    print_time();
    delay_ms(500);
    print_time();

    /*ADC Configuration*/
    // ADC_BASE_Init();

    /*Blynk GPIO with n times:*/
    Startup_Infor(&BUZZER, 5);
    print_time();
    Startup_Infor(&LED1, 5);
    print_time();
    Startup_Infor(&LED2, 5);
    print_time();
    // Startup_Infor(&Motor_0, 1);

    
    GPIO_Write_Peripheral(LoadCell_RS, HIGH);
    delay_ms(200);
    GPIO_Write_Peripheral(LoadCell_RS, LOW);

    
    doorStepHandle.chieu = LOW;
    doorStepHandle.vong = 10;
    Step_Set(&doorStepHandle);
    UART_WriteStr("Calib Door\n");
    print_time();

    while (timeout)
    {
        timeout--;
        Step_Start(&doorStepHandle);
        if(GPIO_Read_Peripheral(DoorSensor) == LOW)
        {
            Step_Hold(&doorStepHandle);
            delay_ms(100);
            Step_Stop(&doorStepHandle);
            UART_WriteStr("Break\n");
            timeout = 0;
        }
        CLRWDT();
    }
    print_time();

    UART_WriteStr("Calib Compress...");
    timeout = 60;
    Motor_Forward_Start(Compress_Motor);
    while (timeout)
    {
        timeout--;
        UART_WriteStr(".");
        GPIO_Toggle(LED1.Port, LED1.Pin);
        delay_ms(500);
        GPIO_Toggle(LED2.Port, LED2.Pin);
        print_time();
        CLRWDT();
    }
    UART_WriteStr("\nDone\n");

    GPIO_Write(LED1.Port, LED1.Pin, LOW);
    GPIO_Write(LED2.Port, LED2.Pin, LOW);
    GPIO_Write(BUZZER.Port, BUZZER.Pin, LOW);
    GPIO_Write(Motor_0.Port, Motor_0.Pin, LOW);
    Step_Stop(&doorStepHandle);
    print_time();
    UART_WriteStr("RESET: OK\n");
}

uint8_t SetMode(void)
{
    uint8_t mode = 0;

    if (GPIO_Read(SW1.Port, SW1.Pin) == HIGH)
    {
        mode = 2;
    }
    else if (GPIO_Read(SW2.Port, SW2.Pin) == HIGH)
    {
        mode = 3;
    }
    
    return mode;
}

