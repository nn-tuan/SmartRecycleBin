#include "main.h"

void main(void)
{
    uint8_t mode = 0;

    MCU_Config();
    mode = SetMode();

    while (1)
    {
        if (mode == 0)
        /*==================================*/
        {
            Loop(&timeSysTick);
        }
        /*==================================*/
        else if (mode != 3)
        {
            /*============== Door test ==============*/
            if (GPIO_Read(SW1.Port, SW1.Pin) == HIGH)
            {
                doorStepHandle.chieu = HIGH;
                doorStepHandle.vong = 400;
                Step_Set(&doorStepHandle);
                Step_Start(&doorStepHandle, &timeSysTick);
            }
            else if (GPIO_Read(SW2.Port, SW2.Pin) == HIGH)
            {
                doorStepHandle.chieu = LOW;
                doorStepHandle.vong = 400;
                Step_Set(&doorStepHandle);
                Step_Start(&doorStepHandle, &timeSysTick);
            }
            else
            {
                Step_Stop(&doorStepHandle);
            }
        }
        /*==================================*/
        else
        {
            /*============== Compress test ==============*/
            if (GPIO_Read(SW1.Port, SW1.Pin) == HIGH)
            {
                Motor_Forward_Start(&Compress_Motor);
            }
            else if (GPIO_Read(SW2.Port, SW2.Pin) == HIGH)
            {
                Motor_Reverse_Start(&Compress_Motor);
            }
            else
            {
                Motor_Stop(&Compress_Motor);
            }
        }
        
        
        /*====== Watchdog Timer Reset ======*/
        if (timeReset_flag != 0)
        {
            timeReset_flag = 0;
        }
        /*==================================*/
    }
}
