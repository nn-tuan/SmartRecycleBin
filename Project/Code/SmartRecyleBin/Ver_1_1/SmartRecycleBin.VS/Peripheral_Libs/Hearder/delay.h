#ifndef DELAY_H
    #define DELAY_H
    #ifndef _REGISTER_H_
        #include "../Hearder/Register.h"
    #endif
    
    void delay_us(uint32_t t_delay); 

    void delay_ms(uint32_t t_delay);   
#endif	/* ADC_H */