#include "../Hearder/delay.h"

void delay_ms(uint32_t t_delay)
{
    while(t_delay > 0)
    {
        t_delay--;
        __delay_ms(1);
    }
}

void delay_us(uint32_t t_delay)
{
    while(t_delay > 0)
    {
        t_delay--;
        __delay_us(1);
    }
}